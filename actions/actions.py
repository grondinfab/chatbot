# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"
import logging
logger = logging.getLogger(__name__)

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet


class ActionCarbonScore(Action):

    def name(self) -> Text:
        return "action_show_carbon_score"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        # Compute carbon score
        s1 = tracker.get_slot("question1")
        s2 = tracker.get_slot("question2")
        s3 = tracker.get_slot("question3")
        s4 = tracker.get_slot("question4")
        s5 = tracker.get_slot("question5")
        carbon_score = s1*s2*52 + s3 + s4*52 + s5
        
        logger.debug(s1,s2,s3,s4,s5,carbon_score)

        dispatcher.utter_message(text="Votre impact carbone est de "+str(carbon_score)+" Kg CO2e par an.")

        if carbon_score<=2000:
            dispatcher.utter_message(text="Bravo ! Vous avez un mode de vie et de consommation responsable. Moins de 2 000 kg CO2, c’est l’objectif des Accords de Paris.")
        else:
            if carbon_score<=4000:
                dispatcher.utter_message(text="Bravo ! Vous êtes sur la voie d’un mode de vie et de consommation responsable. Mais vous pouvez faire mieux en travaillant sur vos points faibles, afin d’atteindre moins de 2 000 kg CO2. C’est l’objectif des Accords de Paris.")
            else:
                if carbon_score<=9000:
                    dispatcher.utter_message(text="Vous pouvez surement faire mieux en vous informant sur vos points faibles, afin d’atteindre moins de 2 000 kg CO2. C’est l’objectif des Accords de Paris, pour être neutre en carbone, c’est-à-dire avoir un équilibre parfait entre les émissions émises et les émissions absorbées !")
                else:
                    dispatcher.utter_message(text="ATTENTION, vous êtes dans le rouge et la planète aussi ! Revoyez en profondeur votre mode de vie pour atteindre moins de 2 000 kg CO2 par an. C’est l’objectif des Accords de Paris, pour sauver la planète d’un trop grand réchauffement climatique.")

        return [SlotSet("carbon_score", carbon_score)]

class ActionResetFormSlots(Action):

    def name(self) -> Text:
        return "action_reset_form_slots"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        return [SlotSet("question1", None), SlotSet("question2", None), SlotSet("question3", None), SlotSet("question4", None), SlotSet("question5", None)]
